import { Component, Input } from '@angular/core';

import { Restaurant } from 'app/restaurants/restaurant/restaurant.model';

@Component({
  selector: 'mt-restaurant',
  templateUrl: './restaurant.component.html'
})
export class RestaurantComponent {

  @Input() restaurant: Restaurant;
}
