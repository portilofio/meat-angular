import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Restaurant } from 'app/restaurants/restaurant/restaurant.model';
import { RestaurantsService } from 'app/restaurants/restaurants.service';
import { ShopingCardService } from './shoping-cart/shoping-card.service';


@Component({
  selector: 'mt-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  viewProviders: [
    ShopingCardService
  ]
})
export class RestaurantDetailComponent implements OnInit {

  restaurant: Restaurant;

  constructor(private restaurantService: RestaurantsService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const idRoute = this.route.snapshot.params['id'];

    this.restaurantService
      .getRestaurantsById(idRoute)
        .subscribe(restaurantLocal => this.restaurant = restaurantLocal);
  }

}
