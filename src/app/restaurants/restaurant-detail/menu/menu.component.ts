import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { MenuItem } from './../menu-item/menu-item.model';
import { RestaurantsService } from './../../restaurants.service';
import { ShopingCardService } from './../shoping-cart/shoping-card.service';

@Component({
  selector: 'mt-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  menus: Observable<MenuItem[]>;

  constructor(private service: RestaurantsService,
              private route: ActivatedRoute,
              private shoppingCartService: ShopingCardService) { }

  ngOnInit() {
    const idParant = this.route.parent.snapshot.params['id'];
    this.menus = this.service.getMenuOfRestaurant(idParant);
  }

  addMenuItem(item: MenuItem): void {
    this.shoppingCartService.addItem(item);
  }
}
