export interface Review {
  name: string;
  rating: number;
  date: Date;
  comments: string;
}
