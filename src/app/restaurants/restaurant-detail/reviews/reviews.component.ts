import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Review } from './review.model';
import {RestaurantsService } from './../../restaurants.service';

@Component({
  selector: 'mt-reviews',
  templateUrl: './reviews.component.html'
})
export class ReviewsComponent implements OnInit {

  reviews: Observable<Review>;

  constructor(private service: RestaurantsService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const idParant = this.route.parent.snapshot.params['id'];
    this.reviews = this.service.getReviewsOfRestaurant(idParant);
  }

}
