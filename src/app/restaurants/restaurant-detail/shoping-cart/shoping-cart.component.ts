import { Component, OnInit } from '@angular/core';

import { ShopingCardService } from './shoping-card.service';
import { CartItem } from './cart-item.model';

@Component({
  selector: 'mt-shoping-cart',
  templateUrl: './shoping-cart.component.html'
})
export class ShopingCartComponent implements OnInit {

  constructor(private shopingCardService: ShopingCardService) { }

  ngOnInit() {
  }

  itens(): CartItem[] {
    return this.shopingCardService.itens;
  }

  total(): number {
    return this.shopingCardService.total();
  }

  clear(): void {
    this.shopingCardService.clear();
  }

  removeItem(item: CartItem): void {
    this.shopingCardService.removeItem(item);
  }
}

