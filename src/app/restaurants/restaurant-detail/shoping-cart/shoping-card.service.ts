import { Injectable } from '@angular/core';

import { CartItem } from './cart-item.model';
import { MenuItem } from './../menu-item/menu-item.model';

@Injectable()
export class ShopingCardService {

    itens: CartItem[] = [];

    constructor() { }

    clear(): void {
        this.itens = [];
    }

    addItem(item: MenuItem): void {
        const foundedItem = this.itens.find((mItem) => mItem.menuItem.id === item.id);
        if (foundedItem) {
            foundedItem.quantity += 1;
        } else {
            this.itens.push(new CartItem(item));
        }
    }

    removeItem(item: CartItem): void {
        this.itens.splice(this.itens.indexOf(item), 1);
    }

    total(): number {
        return this.itens
            .map(item => item.value())
            .reduce((atual, reduce) => reduce += atual, 0);
    }
}
