import { Component, OnInit } from '@angular/core';

import { Restaurant } from 'app/restaurants/restaurant/restaurant.model';
import { RestaurantsService } from 'app/restaurants/restaurants.service';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
})
export class RestaurantsComponent implements OnInit {

  restaurants: Restaurant[];

  constructor(private restService: RestaurantsService) { }

  ngOnInit() {
    this.restService.restaurants()
      .subscribe(restaurants => this.restaurants = restaurants);
  }
}
