import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Restaurant } from 'app/restaurants/restaurant/restaurant.model';
import { ErrorHandler } from 'app/app.error-handler';
import { Review } from 'app/restaurants/restaurant-detail/reviews/review.model';
import { MenuItem } from './restaurant-detail/menu-item/menu-item.model';
import { MEAT_API } from 'app/app.api';

@Injectable()
export class RestaurantsService {

  constructor(private http: Http) { }

  restaurants(): Observable<Restaurant[]> {
    return this.http
            .get(`${MEAT_API}/restaurants`)
              .map(response => response.json())
              .catch(ErrorHandler.handleError);
  }

  getRestaurantsById(id: string): Observable<Restaurant> {
    return this.http
            .get(`${MEAT_API}/restaurants/${id}`)
              .map(response => response.json())
              .catch(ErrorHandler.handleError);
  }

  getReviewsOfRestaurant(id: string): Observable<Review> {
    return this.http
            .get(`${MEAT_API}/restaurants/${id}/reviews`)
              .map(response => response.json())
              .catch(ErrorHandler.handleError);
  }

  getMenuOfRestaurant(id: string): Observable<MenuItem[]> {
    return this.http
            .get(`${MEAT_API}/restaurants/${id}/menu`)
              .map(response => response.json())
              .catch(ErrorHandler.handleError);
  }
}
