import { Routes } from '@angular/router';

import { HomeComponent } from 'app/home/home.component';
import { AboutComponent } from 'app/about/about.component';
import { OrderComponent } from './order/order.component';
import { RESTAURANTS_ROUTE } from 'app/restaurants/restaurants.module';

export const ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  }, ...RESTAURANTS_ROUTE,
  {
    path: 'orders',
    component: OrderComponent
  }

];
